---
title: "Software"
weight: 1
---
Next, we are going to check the software installed to make sure the testbed can support the workshop.
We might not use all of them just yet, but it's good to check this anyways.

### Download `qshuf-check`

I wrote a little tool to check for dependencies.

```bash
export GH_VER=0.1.7
export GH_URL=https://gitlab.com/qnib-golang/qshuf-check/-/releases/v${GH_VER}/downloads
wget -qO- ${GH_URL}/qshuf-check_${GH_VER}_linux_amd64.tar.gz |tar xfz - --directory ~/
```

### Run Check

The tool uses the name of the workshop to check whether all the dependencies are met and you can go ahead.

```bash
export QSHUF_WORKSHOP=container-runtimes-101
~/qshuf-check validate
```
The output shows whether all requirements are met. In the below output we are good to go.

```{ .bash .no-copy}
$ ~/qshuf-check validate
| Status | Name       | Path                  | Version    | Min        | Max        |
| OK     | slurm      | /bin/sinfo            | 1.0.2      | 1.0.2      |            |
| OK     | crun       | /bin/crun             | 1.4.1      | 1.4.1      |            |
| OK     | slurm      | /bin/sinfo            | 21.08.5    | 21.8.0     |            |
| OK     | podman     | /bin/podman           | 4.0.2      | 4.0.0      |            |
| OK     | jq         | /bin/jq               | 1.5        | 1.5.0      |            |
| OK     | docker     | /bin/docker           | 20.10.12   | 20.10.0    |            |
| OK     | skopeo     | /bin/skopeo           | 1.7.0      | 1.6.0      |            |
| OK     | wildq      | /usr/local/bin/wildq  | 1.1.11     | 1.1.11     |            |
```
