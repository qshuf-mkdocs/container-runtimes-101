---
title: "Container 101"
weight: 3
---
Ok, let us start with the basic example of running a OCI container.

Instead of using a single command, let us break the container lifecycle into three phases.

### Image Pull

First we need to download an image (like a tar ball or an OCI image). This is the unextracted file-system for our container with some metadata.

### Snapshot 

Next, we snapshot the downloaded image into an actual container filesystem and create a configuration from the image metadata. Examples of information we extract from metadata:
1. what command to execute? (`RUN echo hello world`)
2. does the container image define environment variables? (`ENV key=val`)
3. Does the image come with labels that help us inform how to run this (e.g. `mpi.version>x.y.z`)

### Run

Once we prepared the container we can actually start it...


## Examples

The subchapters will show different examples of simple, non-HPC examples how that works.
