---
title: "Docker"
weight: 1
---
Ok, let us start with the basic example of running a docker container.

Instead of using the `docker run` command, let us break the container lifecycle into three phases.

### Image Pull

First we download the OCI image [alpine:3.15](https://hub.docker.com/_/alpine) from DockerHub. This uses layers and metadata to optimize the download and reuse layers already present.

```bash
docker pull alpine:3.15
```

The result is one layer being downloaded.

```bash
$ docker pull alpine:3.15
3.15: Pulling from library/alpine
59bf1c3509f3: Pull complete
Digest: sha256:21a3deaa0d32a8057914f36584b5288d2e5ecc984380bc0118285c70fa8c9300
Status: Downloaded newer image for alpine:3.15
docker.io/library/alpine:3.15
```

### Snapshot 
Next, we snapshot the downloaded image into a container filesystem.

```bash
docker create -ti --rm --name alp315 alpine:3.15 cat /etc/os-release
docker container ls -a
```

We created a container snapshot and configuration ready to be started.

```bash
$ docker create -ti --rm --name alp315 alpine:3.15 cat /etc/os-release
8057a8eeb2bc096f33bf5946ce68df8a9e3e7f2b241bbef173842c8c29ff4a5b
$ docker container ls -a
CONTAINER ID   IMAGE         COMMAND                 CREATED          STATUS    PORTS     NAMES
8057a8eeb2bc   alpine:3.15   "cat /etc/os-release"   53 seconds ago   Created             alp315
```

### Run

```bash
docker start -a alp315
```

`docker start` actually spawns the process, attached to stdout (`-a`), and removes the container afterwards (b/c we used `--rm` when creating it).

```bash
$ docker start -a alp315
NAME="Alpine Linux"
ID=alpine
VERSION_ID=3.15.0
PRETTY_NAME="Alpine Linux v3.15"
HOME_URL="https://alpinelinux.org/"
BUG_REPORT_URL="https://bugs.alpinelinux.org/"
```

## One Shot

We can combine all three commands into one: `docker run` with a different image version.

```bash
docker run -ti --rm alpine:3.14 cat /etc/os-release
```

This command will download the iamge, create the snapshot and config, and run the container afterward.

```bash
$ docker run -ti --rm alpine:3.14 cat /etc/os-release
Unable to find image 'alpine:3.14' locally
3.14: Pulling from library/alpine
97518928ae5f: Pull complete
Digest: sha256:635f0aa53d99017b38d1a0aa5b2082f7812b03e3cdb299103fe77b5c8a07f1d2
Status: Downloaded newer image for alpine:3.14
NAME="Alpine Linux"
ID=alpine
VERSION_ID=3.14.3
PRETTY_NAME="Alpine Linux v3.14"
HOME_URL="https://alpinelinux.org/"
BUG_REPORT_URL="https://bugs.alpinelinux.org/"
```
