---
title: "Home"
hide:
- navigation
- toc
---
# Using Container Runtimes 101
This workshop walks you through the basic setup and usage of a vagrant based Slurm cluster.
We'll spin up one headnode and four small compute nodes to explore Slurm on such a small setup.
