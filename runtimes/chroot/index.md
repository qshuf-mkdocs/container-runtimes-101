---
title: "Chroot"
weight: 1
---
The idea of isolating a process from other processes is not new. `chroot` is (in a way) the godfather of it all. It creates a process with it's own root file-system different from the rest.


## Download root filesystem

We'll use an Alpine Linux user-land since it is designed to have a small footprint and is only a couple of MB in size.

```bash
URL=http://dl-cdn.alpinelinux.org/alpine/latest-stable/releases/x86_64
FNAME=$(wget -qO- ${URL}/latest-releases.yaml |awk '/file: alpine-minirootfs/{print $2}') # (1)
mkdir -p rootfs
cd rootfs
wget -qO- "http://dl-cdn.alpinelinux.org/alpine/latest-stable/releases/x86_64/${FNAME}" |tar xfz -
cd ..
```

1. Fetching the latest release via `latest-releases.yaml`s

## Prepare Mounts

`Chroot` will just drop a process into a new root filesystem. The file-systems managed by the kernel are not included and we need to add them in.
Those operations need privileges, so we are using `sudo`. You can already 'feel' that this is a security nightmare... :) 

```bash
sudo mount -t proc /proc ./rootfs/proc
sudo mount -o bind /sys ./rootfs/sys
sudo mount -o bind /dev ./rootfs/dev
sudo mount --make-rslave ./rootfs/sys
sudo mount --make-rslave ./rootfs/dev
```

## Spawn Process


``` mermaid
sequenceDiagram
    autonumber
    participant bash
    Note left of bash: interactive shell of a user
    participant sh
    par fork/exec
        bash->>sh: sudo chroot rootfs sh
    end
     Note right of sh: forked process with different userland
```

Forking a process within the new root file-system requires privileges again.

```bash
sudo chroot rootfs sh
cat /etc/os-release
```

This creates a new process (`sh`) within the alpine userland.

```{ .bash .no-copy}
$ sudo chroot rootfs sh
/ # cat /etc/os-release
NAME="Alpine Linux"
ID=alpine
VERSION_ID=3.15.0
PRETTY_NAME="Alpine Linux v3.15"
HOME_URL="https://alpinelinux.org/"
BUG_REPORT_URL="https://bugs.alpinelinux.org/"
/ #
```

## Discussion

Even though it works and we spawn a process in a different user-land we have major drawbacks.

#### Privileged

We need to setup mounts using `sudo` and we also need to run the `chroot` command as a privileged user.

#### No isolation

`Chroot` does not use any isolation. The process (running as root) is able to see other processes, mount volumes...
```{ .bash .no-copy}
/ # ps -ef|grep container
  751 root      0:06 /usr/bin/containerd
  972 root      0:01 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock
205989 root      0:00 grep container
/ # 
```
... and scary things like adding a new IP address.

```bash
ip addr add 10.128.55.2 dev eth0
ip -o -4 address show
```

This shows a new IP for the interface `eth0`.

```{ .bash .no-copy}
/ # ip addr add 10.128.55.2 dev eth0
/ # ip -o -4 address show
1: lo    inet 127.0.0.1/8 scope host lo\       valid_lft forever preferred_lft forever
2: eth0    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute eth0\       valid_lft 83930sec preferred_lft 83930sec
2: eth0    inet 10.128.55.2/32 scope global eth0\       valid_lft forever preferred_lft forever
3: eth1    inet 192.168.56.10/24 brd 192.168.56.255 scope global noprefixroute eth1\       valid_lft forever preferred_lft forever
4: docker0    inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0\       valid_lft forever preferred_lft forever
```

### Conclusion

`Chroot` is nice, but way to insecure to be considered an option to run processes in separat user-lands.


## CleanUp

To clean up the mounts, let's exit and unmount what we mounted before.

```bash
exit
sudo umount ./rootfs/proc                                                                                               
sudo umount ./rootfs/sys
sudo umount ./rootfs/dev
```
