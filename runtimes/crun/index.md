---
title: "crun"
weight: 3
---


## Root filesystem

We'll use an alpine user-land to run our container. You might already have downloaded one. the snippet below will check for the existance and download 

```
if [[ ! -d rootfs ]];then 
    URL=http://dl-cdn.alpinelinux.org/alpine/latest-stable/releases/x86_64
    FNAME=$(curl -s ${URL}/latest-releases.yaml |awk '/file: alpine-minirootfs/{print $2}')
    mkdir -p rootfs
    cd rootfs
    wget -qO- "http://dl-cdn.alpinelinux.org/alpine/latest-stable/releases/x86_64/${FNAME}" |tar xfz -
    cd ..
else 
  echo "file already downloaded"
fi
```

??? note "Got an ssl error?"
    In case you find yourself having this error:
    ```bash
    curl: symbol lookup error: /lib64/libk5crypto.so.3: undefined symbol: EVP_KDF_ctrl, version OPENSSL_1_1_1b
    ```
    Please unload the `openssl` package using `spack unload openssl`. That is due to the wrong `openssl version` being used.

### Create Config

With the container file-system downloaded and extracted we need to create a OCI configuration.

```bash
rm -f config.json
crun spec --rootless
```

Let's already add a `/home` mount.

```bash
cat config.json \
  |wildq -i json -M '.mounts += [{"destination": "/home","type":"none", "source": "/nfs/home", "options": ["rbind","rw"]}]' \
  |sponge config.json
```

#### Eval Config

##### UID mapping

The configuration defines the UID/GID to be zero, which results in the user being root.

```bash
$ jq '.process.user' config.json
{
  "uid": 0,
  "gid": 0
}
```

!!! note "Crun and rootless"
    It seems that crun does not create the uidMapping below. Might be good to use `runc spec --rootless` instead.

At the same time it is going to map the internal UID `0` to the UID of `alice` on the actual host.

```bash
jq .linux.uidMappings config.json
[
  {
    "containerID": 0,
    "hostID": 2001,
    "size": 1
  }
]
```

This results in files being written from within the container to appear as user `alice`. In fact all the POSIX permissions are honored.

### Execution Flow

``` mermaid
sequenceDiagram
    autonumber
    participant sh
    Note left of sh: interactive shell of a user
    participant bash
    par fork/exec
        sh->>bash: crun run rootfs
    end
    Note right of bash: forked process in isolated namespaces
```

## Run

To create the container we are going to run `crun run` within the directory the `config.json` file is located.

```bash
crun run alpine-cnt
cat /etc/os-release
touch /home/alice/test321
ls -l /home/alice/test321
```

The `touch` command creates a file in the `/home` mount which is shared with the container.

## CleanUp

We just need to exit the container. No special mounts are made outside of that.

```bash
exit
ls -l ~/test321
rm -f ~/test321
```
