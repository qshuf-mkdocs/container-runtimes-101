---
title: "RunC"
weight: 2
---


## Root filesystem

We'll use an alpine user-land to run our container. You might already have downloaded one. the snippet below will check for the existance and download 

```
if [[ ! -d rootfs ]];then 
    URL=http://dl-cdn.alpinelinux.org/alpine/latest-stable/releases/x86_64
    FNAME=$(wget -qO- ${URL}/latest-releases.yaml |awk '/file: alpine-minirootfs/{print $2}')
    mkdir -p rootfs
    cd rootfs
    wget -qO- "http://dl-cdn.alpinelinux.org/alpine/latest-stable/releases/x86_64/${FNAME}" |tar xfz -
    cd ..
else 
  echo "file already downloaded"
fi
```

### Create Config

With the container file-system downloaded and extracted we need to create a OCI configuration.

```bash
rm -f config.json
runc spec --rootless
```

#### Eval Config

##### UID mapping
Let's have a look at the `config.json` created by the `run spec` command.

```bash
jq '.process.user' config.json
```
The configuration defines the UID/GID to be zero, which results in the user being root.

```bash
$ jq '.process.user' config.json
{
  "uid": 0,
  "gid": 0
}
```

At the same time it is going to map the internal UID `0` to the UID of `alice` on the actual host.

```bash
jq .linux.uidMappings config.json
```

Output:

```bash
$ jq .linux.uidMappings config.json
[
  {
    "containerID": 0,
    "hostID": 2001,
    "size": 1
  }
]
```

This results in files being written from within the container to appear as user `alice`. In fact all the POSIX permissions are honored.

### Execution Flow

``` mermaid
sequenceDiagram
    autonumber
    participant sh
    Note left of sh: interactive shell of a user
    participant bash
    par fork/exec
        sh->>bash: runc run rootfs
    end
    Note right of bash: forked process in isolated namespaces
```

## Run

To create the container we are going to run `runc run` within the directory the `config.json` file is located.

```bash
runc run alpine-cnt
cat /etc/os-release
```

Afterwards we exit the container.

```bash
exit
```

## POSIX mount

Now that we ran a contianer in isolation (nothing shared with the host), let us bind-mount the home directory.

```bash
cat config.json \
  |wildq -i json -M '.mounts += [{"destination": "/home","type":"none", "source": "/nfs/home", "options": ["rbind","rw"]}]' \
  |sponge config.json
```

## Create File in HOME

Now we can clearly see the benefit of runc over chroot. Let's run the container first.

```bash
runc run alpine-cnt
```

Next we identify ourself using `whoami`:

```bash
/ # whoami
root
```
Ok, we are root... What happens if we touch a file in `alice` home directory?

```bash
touch /home/alice/test123
ls -l /home/alice/test123
```

It shows to be owned by the user within the container.

```bash
/ # touch /home/alice/test123
/ # ls -l /home/alice/test123
-rw-r--r--    1 root     root             0 Mar 16 11:09 /home/alice/test123
/ #
```

But once we exit container, we will find that the file is actually owned by `alice`.

```bash
exit
ls -l ~/test123
```
Output:

```bash
/ # exit
[alice@headnode ~]$ ls -l ~/test123
-rw-r--r-- 1 alice compute 0 Mar 16 11:09 /nfs/home/alice/test123
```
That's due to the UID mapping used by the rootless container. UID `0` is mapped to UID 2001 from the kernel's perspective.

```bash
$ jq .linux.uidMappings config.json
[
  {
    "containerID": 0,
    "hostID": 2001,
    "size": 1
  }
]
```
