---
title: "Container Runtimes 101"
weight: 4
---
This chapter introduces runtimes and shows some examples of hot to use them.

We are going to start simple and get more special throughout the end of the chapter.

## Flowchart

To illustrate how a container is started, you'll find flowcharts like this.

``` mermaid
sequenceDiagram
    autonumber
    participant sh
    Note left of sh: interactive shell of a user
    participant bash
    par fork/exec
        sh->>bash: bash
    end
     Note right of bash: forked process (at the end in a container)
```

We'll always start with an interactive shell (`sh`) from which a command is issued. This basic case will execute `bash` to start a new interactive shell.
